import React, { Component } from 'react';

class TableUser extends Component {

  constructor(props) {
    super(props)
    this.state = {
      indexEdit: -1,
      usernameChange: '',
      phoneChange: '',
      roleChange: ''
    }
  }

  listSearch = () => {
    return this.props.listUser.filter(element => element.username.includes(this.props.dataSearch))
  }

  editUser = index => {
    this.setState({
      indexEdit: index
    })
  }

  cacelEdit = () => {
    this.setState({
      indexEdit: -1
    })
  }

  changeValue = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  doneEdit = () => {
    this.props.listUser[this.state.indexEdit] = {
      username: this.state.usernameChange,
      phone: this.state.phoneChange,
      role: this.state.roleChange
    }
    this.props.updateList(this.props.listUser)
    this.setState({
      indexEdit: -1
    });
  }

  deleteUser = (key) => {
    this.props.listUser.splice(key, 1)
    this.props.updateList(this.props.listUser)
  }

  renderUser = (value, key) => {
    if (key === this.state.indexEdit) {
      return (
        <tr key={key}>
          <th scope="row">{ key + 1 }</th>
          <td>
            <input
              type="text"
              className="form-control"
              name="usernameChange"
              placeholder={value.username}
              onChange={(event) => this.changeValue(event)}
            />
          </td>
          <td>
            <input
              type="text"
              name="phoneChange"
              className="form-control"
              placeholder={value.phone}
              onChange={(event) => this.changeValue(event)}
            /></td>
          <td>
            <input
              type="text"
              name="roleChange"
              className="form-control"
              placeholder={value.role}
              onChange={(event) => this.changeValue(event)}
            /></td>
          <td>
          <div
            className="btn-group"
            role="group"
            aria-label="Button group with nested dropdown"
          >
            <button
              type="button"
              className="btn btn-success btn-sm"
              onClick={() => this.doneEdit()}
            >
              Done
            </button>
            <button
              type="button"
              className="btn btn-info btn-sm"
              onClick={() => this.cacelEdit()}
            >
              Cancel
            </button>
          </div>
          </td>
        </tr>
      )
    } else {
      return (
        <tr key={key}>
          <th scope="row">{ key + 1 }</th>
          <td>{ value.username }</td>
          <td>{ value.phone }</td>
          <td>{ value.role }</td>
          <td>
          <div
            className="btn-group"
            role="group"
            aria-label="Button group with nested dropdown"
          >
            <button
              type="button"
              className="btn btn-warning btn-sm"
              onClick={() => this.editUser(key)}
            >
              Edit
            </button>
            <button
              type="button"
              className="btn btn-danger btn-sm"
              onClick={() => this.deleteUser(key)}
            >
              Delete
            </button>
          </div>
          </td>
        </tr>
      )
    }
  } 

  showDataUser = () => {
    // console.log(object);
    return (
      this.listSearch().map((value, key) => (
        this.renderUser(value, key)
      ))
    )
  }

  render() {
    return (
      <table className="table">
        <caption>List of users</caption>
        <thead>
          <tr>
            <th scope="col">STT</th>
            <th scope="col">Name</th>
            <th scope="col">Phone</th>
            <th scope="col">Role</th>
            <th scope="col">Task</th>
          </tr>
        </thead>
        <tbody>
          { this.showDataUser() }
        </tbody>
      </table>

    );
  }
}

export default TableUser;