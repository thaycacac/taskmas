import React, { Component } from 'react';

class Search extends Component {
  render() {
    return (
      <div className="input-group">
        <input
          onChange={(data) => this.props.inputSearch(data)}
          type="text"
          placeholder="Search User"
          className="form-control"
          aria-label="Input group example"
          aria-describedby="btnGroupAddon" />
      </div>
    );
  }
}

export default Search;