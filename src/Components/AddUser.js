import React, { Component } from 'react';

class AddUser extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showAddUser: false,
      username: '',
      phone: '',
      rule: ''
    }
  }

  addUser = () => {
    this.props.listUser.push({
      username: this.state.username,
      phone: this.state.phone,
      role: this.state.role
    }) 
    this.props.updateDataAfterAdd(this.props.listUser)
  }

  changeAddUser = (event) => {
    const name = event.target.name
    const value = event.target.value
    this.setState({
      [name]: value
    });

  }

  showButtonAddUser = () => {
    if(this.state.showAddUser === true) {
      return(
        <button
          className="btn btn-danger btn-block"
          onClick={() =>  this.changeStateButton()}
        >
          Close
        </button>
      )
    } else {
      return(
        <button
          className="btn btn-success btn-block"
          onClick={() => this.changeStateButton( )}
        >
          Add User
        </button>
      ) 
    }
  }

  changeStateButton = () => {
    this.setState({
      showAddUser: !this.state.showAddUser
    });
  }

  showForm = () => {
    if(this.state.showAddUser === true) {
      return (
        <div className="card mt-2">
          <div className="card-header">
            Add User
          </div>
          <div className="card-body">
            <form className="text-center">
              <div className="form-group">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Enter user"
                  name="username"
                  onChange={(event) => this.changeAddUser(event)}
                  />
              </div>
              <div className="form-group">
                <input
                  type="text"
                  className="form-control"
                  placeholder="phone"
                  name="phone"
                  onChange={(event) => this.changeAddUser(event)}
                  />
              </div>
              <div className="form-group">
                <input
                  type="text"
                  className="form-control"
                  placeholder="role"
                  name="role"
                  onChange={(event) => this.changeAddUser(event)}
                  />
              </div>
              <button
                type="reset"
                className="btn btn-primary btn-block"
                onClick={() => this.addUser()}
              >Add User
              </button>
            </form>
          </div>
        </div>
      )
    }
  }

  render() {
    return (
      <div>
        { this.showButtonAddUser()  }
        { this.showForm() }
      </div>
    );
  }
}

export default AddUser;