import React, { Component } from 'react';
import '../App.css';
import Search from './Search';
import TableUser from './TableUser';
import AddUser from './AddUser';

class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      dataSearch: '',
      listUser : [
        { id: '1', username: 'thaycacac', phone: '0123456789', role: 'admin'},
        { id: '2', username: 'hoapn', phone: '0123456789', role: 'member'},
        { id: '3', username: 'hiepdq', phone: '0123456789', role: 'member'},
      ] 
    }
  }

  inputSearch = data => {
    this.setState({
      dataSearch: data.target.value
    });
  }

  updateList = data => {
    this.setState({
      listUser: data
    })
  }

  render() {
    return (
      <div className="container mt-5">
        <Search inputSearch = {(data) => this.inputSearch(data)}/>
        <div className="row mt-5">
          <div className="col-9">
            <TableUser
              dataSearch = {this.state.dataSearch}
              listUser = {this.state.listUser}
              updateList = {(data) => this.updateList(data)}
              />
          </div>
          <div className="col-3">
            <AddUser 
              listUser = {this.state.listUser}
              updateDataAfterAdd = {(data) => this.updateList(data)}
            />
          </div>
        </div>
      </div>
    )
  }
}

export default App;
